import re


def format_ref(ref: str):
    result = re.match(r'^\S*vk\.com\/\w+', ref)
    if result:
        index_id = result.group(0).index('.com/')
        return result.group(0)[index_id+5:].replace('id', '').replace('/', '')

    return ref
