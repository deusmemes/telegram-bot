import time

sex_dict = {0: 'Не указано', 1: 'Женский', 2: 'Мужской'}


def format_field(field, value):
    if value == '' or value is None:
        return 'Не указано'

    if field == 'city' or field == 'country':
        return value['title']

    if field == 'last_seen':
        return time.ctime(value['time'])

    if field == 'sex':
        return sex_dict[value]

    if field == 'online':
        if value == 0:
            return 'Не в сети'
        else:
            return 'В сети'

    return value


class User:
    def __init__(self, user_json):
        self.id = user_json['id']
        self.first_name = user_json['first_name']  # string
        self.last_name = user_json['last_name']  # string
        self.online = user_json['online']
        self.last_seen = user_json['last_seen']
        self.status = user_json['status']
        self.sex = user_json['sex']
        self.counters = user_json['counters']

        if 'bdate' in user_json:
            self.bdate = user_json['bdate']
        else:
            self.bdate = None
        if 'city' in user_json:
            self.city = user_json['city']
        else:
            self.city = None
        if 'country' in user_json:
            self.country = user_json['country']
        else:
            self.country = None

    def info_message(self):
        return 'Общая информация о пользователе: \n' \
               'Имя: {0} \n' \
               'Фамилия: {1} \n' \
               'Пол: {2} \n' \
               'Город: {3} \n' \
               'Страна: {4} \n' \
               'Дата рождения: {5} \n' \
               'Статус: {6}'.format(self.first_name, self.last_name,
                                    format_field('sex', self.sex), format_field('city', self.city),
                                    format_field('country', self.country), self.bdate,
                                    self.status)

    def counters_message(self):
        return 'Друзья: {0}\n' \
               'Подписчики: {1}\n' \
               'Аудио: {2}\n' \
               'Видео: {3}\n' \
               'Фотографии: {4}\n' \
               'Интересные страницы: {5}'.format(self.counters['friends'], self.counters['followers'],
                                                 self.counters['audios'], self.counters['videos'],
                                                 self.counters['photos'], self.counters['pages'])

    def online_message(self):
        return 'Текущий статус: {0}\n' \
               'Последняя активность: {1}'.format(format_field('online', self.online),
                                                  format_field('last_seen', self.last_seen))
