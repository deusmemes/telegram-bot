import requests

import config
import utils
from models.User import User


class API:
    def __init__(self, token: str):
        self.Token = token

    def get_user_info(self, user_id):
        ref = utils.format_ref(user_id)
        request = 'https://api.vk.com/method/users.get?user_ids={0}&fields={1}&access_token={2}&lang=ru&v=5.120' \
            .format(ref, ','.join(config.SEARCH_FIELDS), self.Token)

        print(request)

        try:
            response = requests.get(request).json()
            if 'error' in response:
                return False
            print(response)
            user = User(response['response'][0])
            return user
        except Exception as ex:
            print(repr(ex))
            return None

    def help(self):
        return 'Это - бот, позволяющий вывести основную информацию ' \
               'о человеке по профилю вк, сатистику и онлайн-статус.\n' \
               'Для поиска необходимо использовать команду /find'
