import telebot
import config
from API import API

bot = telebot.TeleBot(config.TELEGRAM_BOT_TOKEN)
vk_api = API(config.VK_TOKEN)


@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'Привет, ты написал мне /start')


@bot.message_handler(commands=['help'])
def start_message(message):
    bot.send_message(message.chat.id, vk_api.help())


@bot.message_handler(commands=['find'])
def find_user(message):
    sent = bot.send_message(message.chat.id, 'Введите ссылку или никнейм пользователя:')
    bot.register_next_step_handler(sent, get_info)


@bot.message_handler(commands=[r'\w'])
def err(message):
    bot.send_message(message.chat.id, 'Ничего не могу сказать по этому поводу')


def get_info(message):
    bot.send_message(message.chat.id, 'Ищем пользователя {0}'.format(message.text))
    user = vk_api.get_user_info(message.text)
    if not user:
        bot.send_message(message.chat.id, 'Такого пользователя нет')
        return

    markup = telebot.types.InlineKeyboardMarkup(row_width=1)
    button_online = telebot.types.InlineKeyboardButton('Онлайн статус', callback_data='online{0}'.format(user.id))
    button_counters = telebot.types.InlineKeyboardButton('Статистика', callback_data='stat{0}'.format(user.id))

    markup.add(button_online, button_counters)

    bot.send_message(message.chat.id, user.info_message(), reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    try:
        if call.message:
            if str(call.data).startswith('online'):
                user_id = str(call.data).replace('online', '')
                user = vk_api.get_user_info(user_id)
                bot.send_message(call.message.chat.id, user.online_message())
            elif str(call.data).startswith('stat'):
                user_id = str(call.data).replace('stat', '')
                user = vk_api.get_user_info(user_id)
                bot.send_message(call.message.chat.id, user.counters_message())
    except Exception as ex:
        print(repr(ex))


@bot.message_handler(content_types=['text'])
@bot.message_handler(regexp=r'\/\w+')
def send_text(message):
    bot.send_message(message.chat.id, 'Ничего не могу сказать по этому поводу')


bot.polling(none_stop=True)
